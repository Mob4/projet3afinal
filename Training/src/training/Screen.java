/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package training;

/**
 *
 * @author DL9
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;

public class Screen extends JPanel {
 private BufferedImage img;
 private Point pointStart=null,pointEnd;
 private int type=0;
 private Boolean drawable=false;
 private int rectWidth,rectHeight,x,y;
 
	public int getX() {
	return x;
}
	public int getY() {
	return y;
}
	public int getRectHeight() {
	return rectHeight;
}
	public int getRectWidth() {
	return rectWidth;
}
         public void setDrawable(Boolean state)
        {
            drawable=state;
        }
         public void setType(int tp) {
	type=tp;
 }
	public Screen() {
    /* try {
        // img=ImageIO.read(new File("C:\\Users\\DL9\\Documents\\NetBeansProjects\\yassine\\Training\\photos\\img.png"));
     } catch (IOException ex) {
         Logger.getLogger(Screen.class.getName()).log(Level.SEVERE, null, ex);
     }*/
		
	}
	protected void paintComponent(Graphics g)
	{
            super.paintComponent(g);

            switch(type)             //setting colors depending on type
            {
                case 1:
                    g.setColor(Color.green);
                    break;
                case 2:
                    g.setColor(Color.red);
                    break;
                case 3:
                    g.setColor(Color.blue);
                    break;
            }
		if(drawable==true)
                {g.drawImage(img, 0, 0, img.getWidth(), img.getHeight(), null);
                }
               
            if (pointStart != null) {
                    
			if (pointStart.y<pointEnd.y)
			{	
				if (pointStart.x<pointEnd.x)
				{
					 rectWidth=pointEnd.x-pointStart.x;
					 rectHeight=pointEnd.y-pointStart.y;
					g.drawRect(pointStart.x,pointStart.y,rectWidth,rectHeight);
					x=pointStart.x;
					y=pointStart.y;
				}
				else
				{
					rectWidth=pointStart.x-pointEnd.x;
					rectHeight=pointEnd.y-pointStart.y;
					g.drawRect(pointEnd.x,pointStart.y,rectWidth,rectHeight);
					x=pointEnd.x;
					y=pointStart.y;
				}
			}
			else
			{	
				if (pointStart.x<pointEnd.x)
					{
					rectWidth=pointEnd.x-pointStart.x;
					rectHeight=pointStart.y-pointEnd.y;
					g.drawRect(pointStart.x,pointEnd.y,rectWidth,rectHeight);
					x=pointStart.x;
					y=pointEnd.y;
					}
				
				else{
					rectWidth=pointStart.x-pointEnd.x;
					rectHeight=pointStart.y-pointEnd.y;
					g.drawRect(pointEnd.x,pointEnd.y,rectWidth,rectHeight);
					x=pointEnd.x;
					y=pointEnd.y;
					}
			}
		}
              System.out.println("width "+this.getRectWidth());
	}
	
	public void setImg(BufferedImage im)
	{
		img=im;
                this.pointStart=null;// this avoids getting the same rectangle drawn over a new picture. finnaly found the bug ouf!
                this.revalidate();
		this.repaint();
	}
	
	{addMouseListener(new MouseAdapter() {
        public void mousePressed(MouseEvent e) {
            pointStart = e.getPoint();
           
        }

        public void mouseReleased(MouseEvent e) {
        	pointEnd = e.getPoint();
                paintComponent(getGraphics());
            
        }
    });
    addMouseMotionListener(new MouseMotionAdapter() {
        public void mouseMoved(MouseEvent e) {
            
        }

        public void mouseDragged(MouseEvent e) {
         
            
        }
    });
}
	

}
