package training;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlData {
	private DocumentBuilderFactory docFactory;
	private DocumentBuilder docBuilder ;
	private Document document;
	private Element rootElement,components;
	private String imageId;
	
	
	XmlData(String imgId)
	{

		try {
			   imageId=imgId;
			
			   // document skull generation
			   
			 docFactory = DocumentBuilderFactory.newInstance();
			 docBuilder = docFactory.newDocumentBuilder();
			 document = docBuilder.newDocument();
			 
			 rootElement = document.createElement("Image");  // root element (image)
				document.appendChild(rootElement);
				
				Attr attr = document.createAttribute("id");  // attribution de l'id de l'image
				attr.setValue(imageId.toString());
				rootElement.setAttributeNode(attr);
				
				components = document.createElement("Components");  // components
				rootElement.appendChild(components);
				
				
				
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	public void addElement(int elemType,int width,int height,Integer x,Integer y)
	{
            System.out.println("width received "+width);
            System.out.println("height received "+height);
		String type;
            switch (elemType) {
                case Types.IMAGE:
                    type="Img";
                    break;
                case Types.TITRE:
                    type="Titre";
                    break;
                case Types.LETTRINE:
                    type="Lettrine";
                    break;
                default:
                    type="Paragraphe";
                    break;
                
                
            }
		
		try {
			Element component = document.createElement("component");  // component
			components.appendChild(component);
			
			Element tp = document.createElement("type");             //Type
			tp.appendChild(document.createTextNode(type));
			component.appendChild(tp);
			
			Element rectWidth = document.createElement("RectangleWidth");    //Rectangle width
                        System.out.println("width received "+Integer.toString(width));
			rectWidth.appendChild(document.createTextNode(Integer.toString(width)));
			component.appendChild(rectWidth);
			
			Element rectHeight = document.createElement("RectangleHeight");    //Rectangle height
			rectHeight.appendChild(document.createTextNode(Integer.toString(height)));
			component.appendChild(rectHeight);
			
			Element pointX = document.createElement("PointX");    //Rectangle height
			pointX.appendChild(document.createTextNode(x.toString()));
			component.appendChild(pointX);
			
			Element pointY = document.createElement("PointY");    //Rectangle height
			pointY.appendChild(document.createTextNode(y.toString()));
			component.appendChild(pointY);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void genFile()
	{
		try {
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(new File("C:\\Users\\DL9\\Desktop\\XML-Training-Data\\"+imageId.toString()+".xml"));

			transformer.transform(source, result);

			System.out.println("File saved!");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
