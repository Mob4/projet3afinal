# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 13:29:22 2017

@author: DL9
"""

from __future__ import print_function
from wand.image import Image

#nb le chemin du fichier source
 
with Image(filename='test1/source1.pdf',resolution=30) as img: #30 == low quality(utilisée pour l'app java) 60== high quality utilisé pour le traitement de l'img
        print('pages = ', len(img.sequence))
        img.compression_quality = 99
        with img.convert('png') as converted:
            converted.save(filename='test1/result/page_n.png')    #nb le chemin des results